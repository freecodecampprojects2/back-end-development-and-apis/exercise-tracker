const { Router } = require("express");
const { User, Exercise } = require("../models/user");

async function createUser(usr) {
  try {
    // should catch TypeError if it doesent exist else returns the id
    const exists = await User.exists({ username: usr });
    const user = await User.findById(exists._id).select("username _id");
    return user;
  } catch (err) {
    if (err instanceof TypeError) {
      const { username, _id } = await User.create({ username: usr });
      console.log({ username, _id });
      return { username, _id };
    }
    throw err;
  }
}

async function getAllUsers() {
  try {
    return await User.find({}, "_id username");
  } catch (err) {
    throw err;
  }
}

async function addExercise(exercise) {
  const { description, duration, date } = exercise;
  const userId = exercise._id;
  try {
    const user = await User.findById(userId);
    const xercise = await Exercise.create({
      description,
      duration,
      date,
      user,
    });
    user.log.push(xercise);
    await user.save();
    const { username } = user;
    return { username, description, duration, date };
  } catch (err) {
    throw err;
  }
}

function queryBuilder(params) {
  const result = { user: { _id: params.id } };
  const from = params.from === undefined ? false : { $gte: params.from };
  const to = params.to === undefined ? false : { $lte: params.to };
  if (from) result.date = { ...result.date, ...from };
  if (to) result.date = { ...result.date, ...to };
  return result;
}

async function getLogs(params) {
  const { id } = params;
  try {
    const user = await User.findById(id).select("username _id");
    const exercises =
      params.limit === undefined
        ? await Exercise.find(queryBuilder(params))
            .select("description duration date")
            .exec()
        : await Exercise.find(queryBuilder(params))
            .limit(params.limit)
            .select("description duration date")
            .exec();
    const log = exercises.map((ex) => {
      return {
        description: ex.description,
        duration: ex.duration,
        date: ex.date.toDateString(),
      };
    });
    return {
      user: user.username,
      _id: user._id,
      count: log.length,
      log,
    };
  } catch (err) {
    throw err;
  }
}

const router = new Router();

router.post("/api/users", async (req, res) => {
  const { username } = req.body;
  try {
    const user = await createUser(username);
    res.status(200).json(user);
  } catch (err) {
    res.status(400).json({ error: "Bad Input" });
  }
});

router.get("/api/users", async (req, res) => {
  try {
    const users = await getAllUsers();
    res.status(200).json(users);
  } catch (err) {
    res.status(404).json({ error: "Not Found" });
  }
});

router.post("/api/users/:_id/exercises", async (req, res) => {
  try {
    const exercise = await addExercise(req.body);
    res.status(200).json(exercise);
  } catch (err) {
    res.status(400).json({ error: "Bad Input" });
  }
});

router.get("/api/users/:id/logs", async (req, res) => {
  const { id } = req.params;
  const { from, to, limit } = req.query;
  try {
    const result = await getLogs({ id, from, to, limit });
    res.status(200).json(result);
  } catch (err) {
    res.status(400).json({ error: "Bad Input" });
  }
});

module.exports = router;

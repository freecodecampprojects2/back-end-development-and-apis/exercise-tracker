const express = require("express");
const app = express();
const cors = require("cors");
const dbConnection = require("./db");

app.use(cors());
app.use(express.static("public"));
app.use(express.urlencoded({ extended: true }));
app.get("/", (req, res) => {
  res.sendFile(__dirname + "/views/index.html");
});

app.use(require("./api/user"));

const listener = app.listen(process.env.PORT || 3000, () => {
  console.log("Your app is listening on port " + listener.address().port);
});

function handleShutdown() {
  console.log("Closing db connection...");
  dbConnection.close();
  console.log("Stopping server...");
  listener.close();
  console.log("Exiting.");
  process.kill(process.pid, "SIGUSR2");
}

process.on("SIGINT", handleShutdown);

process.once("SIGUSR2", (_) => {
  setTimeout(handleShutdown, 5000);
  setTimeout(() => console.log("Timing out"), 99999);
});
